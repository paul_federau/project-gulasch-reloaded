var { driver } = require('@rocket.chat/sdk');
var schedule = require('node-schedule');
var fetch = require('node-fetch');
var respmap  = require('./reply');
const { username } = require('@rocket.chat/sdk/dist/lib/settings');

// Environment Setup
var HOST = process.env.CHAT_HOST;
var USER = process.env.CHAT_USER;
var PASS = process.env.CHAT_PASSWORD;
var SSL = 'true';
var myUserId;

// Bot configuration
var runbot = async () => {
    var conn = await driver.connect({ host: HOST, useSsl: SSL })
    myUserId = await driver.login({ username: USER, password: PASS });
    var subscribed = await driver.subscribeToMessages();
    console.log('subscribed');
    var msgloop = await driver.reactToMessages( processMessages );
    console.log('connected and waiting for messages');
    var messageRule = new schedule.RecurrenceRule();
    messageRule.tz = 'Europe/Berlin';
    messageRule.hour = 10;
    messageRule.minute = 30;
    messageRule.dayOfWeek = new schedule.Range(1,5);
    schedule.scheduleJob(messageRule, async function () {
        generateMittag();
    });
    var importRule = new schedule.RecurrenceRule();
    importRule.tz = 'Europe/Berlin';
    importRule.hour = 10;
    importRule.minute = 0;
    importRule.dayOfWeek = [1];
    schedule.scheduleJob(importRule, async function () {
        await fetch("http://web:8000/api/data-import/");
    });

}

// Process messages
var processMessages = async(err, message, messageOptions) => {
    if (!err) {
        if (message.u._id === myUserId) return;
        var roomname = await driver.getRoomName(message.rid);

        var response;
        if (message.msg in respmap) {
            var user = {
                "username": message.u.username,
                "exists": false,
            };
            resp = await fetch("http://web:8000/api/botuser-list/")
            .then(resp => resp.json());
            
            for (let i = 0; i < resp.length; i++) {
                if (resp[i].username == message.u.username) {
                    user.exists = true;
                };
            };
            switch (message.msg) {
                case '!sub':
                    if (user.exists) {
                        data = {
                            "username": user.username,
                            "active": true,
                        };
                        postData(`http://web:8000/api/botuser-update/${user.username}/`, data);
                    } else {
                        data = {
                            "username": user.username,
                            "active": true,
                        };
                        postData(`http://web:8000/api/botuser-create/`, data);
                    };
                    break
                case '!unsub':
                    data = {
                        "username": user.username,
                        "active": false,
                    };
                    postData(`http://web:8000/api/botuser-update/${user.username}/`, data);
                    break;
                case '!mittag':
                    generateMittag(user.username, c=true);
                    break;
            };
            response = respmap[message.msg]
        } else {
            response = 'Ali nix schuld. Alles kaputt. Bitte gib doch ´!help´ ein, um alle verfügbaren Commands zu sehen.';
        };
        await driver.sendToRoomId(response, message.rid)
    };
};

// Erzeugen der Nachricht, in der die Hauptspeisen drinnen stehen
var generateMittag = async (u=false, c=false) => {
    try {
        var date = new Date().toISOString().split('T')[0];
        var users = await fetch('http://web:8000/api/botuser-list/')
        .then((users) => users.json());
        var mittag = await fetch(`http://web:8000/api/date-detail/${date}/`)
        .then((resp) => resp.json());
        var msg;
        if (c) {
            msg = '';
        } else {
            msg = 'Heute gibt es zum Mittag:\n\n';
        };
        for (let i = 0; i < mittag.foods.length; i++) {
            msg += mittag.foods[i].name + '\n\n'
        };
        if (u) {
            await driver.sendDirectToUser(content=msg, user=u);
        } else {
            for (let i = 0; i < users.length; i++) {
                if (users[i].active) {
                    await driver.sendDirectToUser(content=msg, user=users[i].username);  
                };
            };
        };
    } catch (e) {
        console.log(e);
    };
    
};

var postData = async (url, data) => {
    fetch(url, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(data),
    })
    .then(resp => resp.json())
    .then(data => {
        console.log('Success:', data);
    })
    .catch((error) => {
        console.error('Error', error);
    });
};


runbot()