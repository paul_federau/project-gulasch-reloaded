const respmap = {
    "!help" : `
        !help
            Ruft die Hilfe auf
        \n!sub
            Aktivieren der Benachrichtigungen durch Project Gulasch
        \n!unsub
            Deaktivieren der Benachrichtigungen durch Project Gulasch
        \n!mittag
            Zeigt dir sofort das Mittagessen des heutigen Tages an`,
    "!sub" : `
        Du erhälst ab jetzt Benachrichtigungen für den aktuellen Speiseplan.`,
    "!unsub" : `
        Ab jetzt erhälst du keine Benachrichtigungen mehr.`,
    "!mittag" : `
        Heute gibt es zum Mittag:\n\n`,
};

module.exports = respmap;