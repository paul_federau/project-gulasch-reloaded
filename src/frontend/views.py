from django.shortcuts import render
from api.models import Dates
from datetime import date


def showMenu(request):
    curDate = date.today()
    dates = list(Dates.objects.filter(date=curDate))
    context = {
        'dates': dates,
    }
    return render(request, 'frontend/index.html', context)
