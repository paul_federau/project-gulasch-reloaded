from django.db import models
from django.db.models.signals import post_delete
from django.dispatch import receiver


class Main(models.Model):
    name = models.CharField(max_length=255,unique=True,null=False)

    def __str__(self):
        return self.name


class Veggie(models.Model):
    name = models.CharField(max_length=255,unique=True,null=False)

    def __str__(self):
        return self.name


class Saturation(models.Model):
    name = models.CharField(max_length=255,unique=True,null=False)

    def __str__(self):
        return self.name


class Dates(models.Model):
    date = models.DateField(unique=True)
    foods = models.ManyToManyField(Main)
    vegetables = models.ManyToManyField(Veggie)
    saturations = models.ManyToManyField(Saturation)

    def __str__(self):
        return str(self.date)


class MenuFiles(models.Model):
    name = models.CharField(max_length=10, unique=True)
    pdf = models.FileField(upload_to='pdfs')

    def __str__(self):
        return str(self.name)


class BotUser(models.Model):
    username = models.CharField(max_length=200, unique=True)
    active = models.BooleanField(default=False)

    def __str__(self):
        return str(self.username)


@receiver(post_delete, sender=MenuFiles)
def autoDeleteFile(sender, instance, **kwargs):
    instance.pdf.delete(False)