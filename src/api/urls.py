from django.urls import path
from . import views

urlpatterns = [
    path('food-list/', views.foodList, name='food-list'),
    path('food-create/', views.foodCreate, name='food-create'),
    path('food-update/<str:name>/', views.foodUpdate, name='food-update'),
    path('food-delete/<str:name>/', views.foodDelete, name='food-delete'),
    path('veggie-list/', views.veggieList, name='veggie-list'),
    path('veggie-create/', views.veggieCreate, name='veggie-create'),
    path('veggie-update/<str:name>/', views.veggieUpdate, name='veggie-update'),
    path('veggie-delete/<str:name>/', views.veggieDelete, name='veggie-delete'),
    path('saturation-list/', views.saturationList, name='saturation-list'),
    path('saturation-create/', views.saturationCreate, name='saturation-create'),
    path('saturation-update/<str:name>/', views.saturationUpdate, name='saturation-update'),
    path('saturation-delete/<str:name>/', views.saturationDelete, name='saturation-delete'),
    path('date-list/', views.dateList, name='date-list'),
    path('date-detail/<str:date>/', views.dateDetail, name='date-detail'),
    path('date-create/', views.dateCreate, name='date-create'),
    path('date-update/<str:date>/', views.dateUpdate, name='date-update'),
    path('date-delete/<str:date>/', views.dateDelete, name='date-delete'),
    path('data-import/', views.dataImport, name='data-import'),
    path('botuser-list/', views.botUserList, name='botuser-list'),
    path('botuser-create/', views.botUserCreate, name='botuser-create'),
    path('botuser-update/<str:username>/', views.botUserUpdate, name='botuser-update'),
    path('botuser-delete/<str:username>/', views.botUserDelete, name='botuser-delete'),
]
