import csv
import requests
import os
import re
from datetime import timedelta, datetime
from rest_framework.decorators import api_view
from rest_framework.response import Response
from .serializers import (
    DatesSerializer,
    MainSerializer,
    SaturationSerializer,
    VeggieSerializer,
    BotUserSerializer,
)
from .models import Dates, Main, Veggie, Saturation, MenuFiles, BotUser
from django.core.files import File
from django.http import HttpResponse
from tabula import convert_into
from django.conf import settings


# Main Food Section
@api_view(["GET"])
def foodList(request):
    foods = Main.objects.all()
    serializer = MainSerializer(foods, many=True)
    return Response(serializer.data)


@api_view(["POST"])
def foodCreate(request):
    serializer = MainSerializer(data=request.data)

    if serializer.is_valid():
        serializer.save()
    return Response(serializer.data)


@api_view(["POST"])
def foodUpdate(request, name):
    food = Main.objects.get(name=name)
    serializer = MainSerializer(instance=food, data=request.data)

    if serializer.is_valid():
        serializer.save()
    return Response(serializer.data)


@api_view(["DELETE"])
def foodDelete(request, name):
    food = Main.objects.get(name=name)
    food.delete()
    return Response("Item successfully deleted!")


# Veggie Section
@api_view(["GET"])
def veggieList(request):
    veggies = Veggie.objects.all()
    serializer = VeggieSerializer(veggies, many=True)
    return Response(serializer.data)


@api_view(["POST"])
def veggieCreate(request):
    serializer = VeggieSerializer(data=request.data)

    if serializer.is_valid():
        serializer.save()
    return Response(serializer.data)


@api_view(["POST"])
def veggieUpdate(request, name):
    veggie = Veggie.objects.get(name=name)
    serializer = VeggieSerializer(instance=veggie, data=request.data)

    if serializer.is_valid():
        serializer.save()
    return Response(serializer.data)


@api_view(["DELETE"])
def veggieDelete(request, name):
    veggie = Veggie.objects.get(name=name)
    veggie.delete()
    return Response("Item successfully deleted!")


# Saturation Section
@api_view(["GET"])
def saturationList(request):
    saturations = Saturation.objects.all()
    serializer = SaturationSerializer(saturations, many=True)
    return Response(serializer.data)


@api_view(["POST"])
def saturationCreate(request):
    serializer = SaturationSerializer(data=request.data)

    if serializer.is_valid():
        serializer.save()
    return Response(serializer.data)


@api_view(["POST"])
def saturationUpdate(request, name):
    saturation = Saturation.objects.get(name=name)
    serializer = SaturationSerializer(instance=saturation, data=request.data)

    if serializer.is_valid():
        serializer.save()
    return Response(serializer.data)


@api_view(["DELETE"])
def saturationDelete(request, name):
    saturation = Saturation.objects.get(name=name)
    saturation.delete()
    return Response("Item successfully deleted!")


# Dates Section
@api_view(["GET"])
def dateList(request):
    dates = Dates.objects.all()
    serializer = DatesSerializer(dates, many=True)
    return Response(serializer.data)


@api_view(["GET"])
def dateDetail(request, date):
    curDate = Dates.objects.get(date=date)
    serializer = DatesSerializer(curDate, many=False)
    return Response(serializer.data)


@api_view(["POST"])
def dateCreate(request):
    serializer = DatesSerializer(data=request.data)

    if serializer.is_valid():
        serializer.save()
    return Response(serializer.data)


@api_view(["POST"])
def dateUpdate(request, date):
    date = Dates.objects.get(date=date)
    serializer = SaturationSerializer(instance=date, data=request.data)

    if serializer.is_valid():
        serializer.save()
    return Response(serializer.data)


@api_view(["DELETE"])
def dateDelete(request, date):
    date = Dates.objects.get(date=date)
    date.delete()
    return Response("Item successfully deleted!")


# BotUser Section
@api_view(["GET"])
def botUserList(request):
    botUsers = BotUser.objects.all()
    serializer = BotUserSerializer(botUsers, many=True)
    return Response(serializer.data)


@api_view(["POST"])
def botUserCreate(request):
    serializer = BotUserSerializer(data=request.data)

    if serializer.is_valid():
        serializer.save()
    return Response(serializer.data)


@api_view(["POST"])
def botUserUpdate(request, username):
    botUser = BotUser.objects.get(username=username)
    serializer = BotUserSerializer(instance=botUser, data=request.data)

    if serializer.is_valid():
        serializer.save()
    return Response(serializer.data)


@api_view(["DELETE"])
def botUserDelete(request, username):
    botUser = BotUser.objects.get(username=username)
    botUser.delete()
    return Response("Item successfully deleted!")


# Data Import API Call
@api_view(["GET"])
def dataImport(request):
    PDF2DB()
    return Response("Speiseplan erfolgreich importiert")


# Custom Utility functions
def PDF2DB():
    temp = "Test"
    # Base URL für Speiseplan + Pfad für Zertifikat der Website
    url = "https://svr-intranet.gzlz.lan/api/v1/files?f=13"
    cert = "./cert.crt"

    # Abfrage nach den aktuell zur Verfügung stehenden Speiseplänen im Intranet
    menus = requests.get(url, verify=cert).json()

    # Start der Verarbeitung jedes gefundenen Speiseplans
    for menu in menus:
        # Generieren der spezifischen Speiseplan URL und nachfolgender Download als temp.pdf
        dlURL = "https://svr-intranet.gzlz.lan/api/v1/files/download/{uuid}".format(
            uuid=menu["uuid"]
        )
        req = requests.get(dlURL, verify=cert)
        with open("temp.pdf", "wb") as f:
            f.write(req.content)

        # Konvertierung von PDF zu CSV
        convert_into("temp.pdf", "temp.csv")

        # Verbesserung der Datenqualität durch Filtern und Ersetzen
        csvData = []
        with open("temp.csv") as f:
            reader = csv.reader(f)
            for row in reader:
                data = []
                for content in row:
                    # Regex um Newline Char zu entfernen
                    rex = re.compile(r"\n")
                    content = rex.sub(" ", content)
                    # Regex um Zahlen für Zusatzstoffe loszuwerden
                    rex = re.compile(r"(\d{1,2}.\d[)])|(\d{1,2}[)])")
                    content = rex.sub("", content)
                    # Regex um Geldbeträge zu beseitigen weil Who Cares
                    rex = re.compile(r"\d{1},\d{2} [€]")
                    content = rex.sub("", content)
                    data.append(content)
                csvData.append(data)
        # Hier werden Zeilen und Spalten der csv getauscht
        # Auch das ist zur besseren Weiterverarbeitung
        csvData = [list(tup) for tup in zip(*csvData)][1:]

        # Startdatum der aktuellen Woche
        startDate = datetime.strptime(csvData[0][0][-10:], "%d.%m.%Y")

        for day in csvData:
            # Datum als Datenbankeintrag anlegen wenn noch nicht vorhanden
            try:
                d = Dates(date=startDate + timedelta(days=csvData.index(day)))
                d.save()
            except Exception as e:
                pass

            # Aktuell zu bearbeitendes Datum als Datenbankobjekt abfragen
            date = Dates.objects.get(
                date=startDate + timedelta(days=csvData.index(day))
            )

            for item in day:
                # Hauptspeisen in DB speichern und dem Datum zuordnen
                if day.index(item) > 0 and day.index(item) < 4:
                    if item != "":
                        try:
                            p = Main(name=item)
                            p.save()
                        except Exception:
                            pass
                        if not date.foods.filter(name=item):
                            date.foods.add(Main.objects.get(name=item))

                # Gemüsebeilagen in DB speichern und dem Datum zuordnen
                elif day.index(item) >= 4 and day.index(item) < 6:
                    if item != "":
                        try:
                            p = Veggie(name=item)
                            p.save()
                        except Exception:
                            pass
                        if not date.vegetables.filter(name=item):
                            date.vegetables.add(Veggie.objects.get(name=item))

                # Sättigungsbeilagen in DB speichern und dem Datum zuordnen
                elif day.index(item) >= 6 and day.index(item) < 8:
                    if item != "":
                        try:
                            p = Saturation(name=item)
                            p.save()
                        except Exception:
                            pass
                        if not date.saturations.filter(name=item):
                            date.saturations.add(Saturation.objects.get(name=item))

        # Speichern des Speiseplans als PDF in DB
        with open("temp.pdf", "rb") as f:
            filename = str(startDate)[:10]
            if not MenuFiles.objects.filter(name=filename):
                try:
                    doc = MenuFiles(name=filename)
                    doc.pdf.save(filename + ".pdf", File(f))
                    doc.save()
                except Exception:
                    pass

        # Löschen der temporären Dateien
        os.remove("temp.pdf")
        os.remove("temp.csv")
