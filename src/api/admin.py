from django.contrib import admin
from .models import Main, Veggie, Saturation, Dates, MenuFiles, BotUser

admin.site.register(Main)
admin.site.register(Veggie)
admin.site.register(Saturation)
admin.site.register(Dates)
admin.site.register(MenuFiles)
admin.site.register(BotUser)