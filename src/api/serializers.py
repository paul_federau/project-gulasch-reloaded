from rest_framework import serializers
from .models import Main, Veggie, Saturation, Dates, BotUser

class MainSerializer(serializers.ModelSerializer):
    class Meta:
        model = Main
        fields = '__all__'


class VeggieSerializer(serializers.ModelSerializer):
    class Meta:
        model = Veggie
        fields = '__all__'

class SaturationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Saturation
        fields = '__all__'

class DatesSerializer(serializers.ModelSerializer):
    foods = MainSerializer(many=True, read_only=True)
    vegetables = VeggieSerializer(many=True, read_only=True)
    saturations = SaturationSerializer(many=True, read_only=True)
    class Meta:
        model = Dates
        fields = 'date', 'foods', 'vegetables', 'saturations'


class BotUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = BotUser
        fields = '__all__'