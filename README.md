# Project Gulasch Reloaded

Neuauflage des berühmten Project Gulasch.

# Umgebung konfigurieren
Im Docker Compose File müssen die Pfade bei den Volumes angepasst werden

Docker-Compose braucht ein project.env File, welcher genauso aussieht
```
POSTGRES_USER=
POSTGRES_PASSWORD=
POSTGRES_DB=

DATABASE=
DATABASE_HOST=
DATABASE_PORT=

SECRET_KEY=

DEBUG=
```